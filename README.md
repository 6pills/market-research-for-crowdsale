# README #

This is a market research done specifically for the Floatlands crowdsale.

### What this repository contains? ###

* Contains data
* Contains calculations (done with python)
* Contains charts

### Details ###

* Research was done with the [Anaconda](https://anaconda.org/). Anaconda is Python based data mining software.
* Data was pulled from the [SteamSpy](http://steamspy.com/). It requires pay-wall for full data, free access has only 2 weeks of free data.
* Data is in JSON format, calculation was made with Python. 
* Charts were drawn with the [Plotly](https://anaconda.org/plotly/plotly)
* Colors were choosen from the [Color Lover](https://plot.ly/ipython-notebooks/color-scales/)

# Charts #

### Owners through time ###

This chart shows game ownership over time. Time series are not alligned to the game release date.

![Alt text](https://bytebucket.org/6pills/market-research-for-crowdsale/raw/523e8bd633b544b2f32d9a00addda3258a590201/owners_through_time.png "Owners")

The Unturned game was excluded in following charts because it's a free to play game (hence why it has so many owners).

### Gross sales over time ###

Time series are alligned to game release date, so we can see their performance more easily.
Gross sales do not include operational costs, steam cut, taxes. 
 
![Alt text](https://bytebucket.org/6pills/market-research-for-crowdsale/raw/902c35dd0d6b1b8420539f0a6ee35ea9df4a846f/gross_sales.png "Gross sales")

### Profit projection ###

This chart is a projection of a potential profits for crowdsale participants, nominated in %. It includes steam cut + taxes (35%). 

Time series are alligned to game release date. Since price of games change through time (Steam sales) we took that into account for more accurate projection.

![Alt text](https://bytebucket.org/6pills/market-research-for-crowdsale/raw/523e8bd633b544b2f32d9a00addda3258a590201/profit_since_release.png "Profit")

Similar chart as before with PUBG included. 
We included PUBG because it performed very well. This is the reason why we want to implement this type of gameplay (Battle Royale).

PUBG extrapolation line is horizontal, because it overperformed and went over the chart.

![Alt text](https://bytebucket.org/6pills/market-research-for-crowdsale/raw/82331987d1a48cadfd983dcedd56f55e7e3a321c/profit_since_release_PUBG_inc.png "Profit - PUBG included")

